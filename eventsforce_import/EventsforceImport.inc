<?php

class EventsforceImport {
  private $nodeType = 'eventsforce_event';
  private $count = 0;

  function __construct($event_type = 'live') {
    // Get the events from eventsforce.
    $this->get($event_type);

    // If some events are found, import them into Drupal.
    if ($this->events) {
      $this->import();
    }
  }

  /**
   * Get the events from eventsforce.
   *
   * @param string $event_type
   *   The type of event (e.g. live, notlive, archived etc).
   */
  function get($event_type) {
    $this->events = eventsforce_api_get_all_events($event_type);
  }

  /**
   * Import the events into Drupal nodes.
   */
  private function import() {
    foreach ($this->events as $event) {
      // Does this event already exist within Drupal?
      $nid = self::eventExist($event);

      // Create a new node or update an existing one.
      if (!$nid) {
        self::create($event);
      }
      else {
        if (variable_get('eventsforce_import_update_existing', 1)) {
          self::update((int) $nid, $event);
        }
      }

      $this->count++;
    }
  }

  /**
   * Check if an event already exists as a node.
   */
  private function eventExist($event) {
    // Search for an eventsforce_event node with this eventsforce ID.
    return db_select('field_data_eventsforce_id', 'id')
      ->fields('id', array('entity_id'))
      ->condition('bundle', $this->nodeType)
      ->condition('eventsforce_id_value', $event->eventID)
      ->execute()
      ->fetchField();
  }

  /**
   * Create a new node.
   */
  private function create($event) {
    // Create a new node within Drupal.
    $entity = entity_create('node', array('type' => $this->nodeType));
    $wrapper = entity_metadata_wrapper('node', $entity);

    // Mirror the eventsforce data to fields.
    $wrapper->title->set($event->eventName);
    $wrapper->eventsforce_url->set($event->detailsURL);
    $wrapper->eventsforce_id->set($event->eventID);
    $wrapper->eventsforce_status->set($event->eventStatus);
    $wrapper->eventsforce_date->set(array(
      'value' => strtotime($event->eventStartDateTime),
      'value2' => strtotime($event->eventEndDateTime)
    ));
    $wrapper->eventsforce_venue->set($event->venueName);

    // Save the node.
    $wrapper->save();
  }

  /**
   * Update a node.
   *
   * @param integer $nid
   *   The nid of the existing node.
   *
   * @param object $event
   *   The event from eventsforce.
   */
  private function update($nid, $event) {
    $node = node_load($nid);
    $wrapper = entity_metadata_wrapper('node', $node);

    // Update any details. Some details such as URL and ID will not be updated.
    $wrapper->title->set($event->eventName);
    $wrapper->eventsforce_status->set($event->eventStatus);
    $wrapper->eventsforce_date->set(array(
      'value' => strtotime($event->eventStartDateTime),
      'value2' => strtotime($event->eventEndDateTime)
    ));
    $wrapper->eventsforce_venue->set($event->venueName);

    // Update the node.
    $wrapper->save();
  }
}
